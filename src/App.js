import React, { Component } from 'react'
import { Table } from 'semantic-ui-react';
import { Button } from 'semantic-ui-react';
import './Table.css'
import 'semantic-ui-css/semantic.min.css'
import AddProduct from './Component/AddProduct.js';
import EditProduct from './Component/EditProduct.js';
import axios from 'axios';
import Swal from 'sweetalert2'


var Ip = `http://18.216.87.100`
var Port = `3000`

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: {
        _id: '',
        name: '',
        images: [],
        images2: [],
        cartoon_capicity: '',
        category_id: '',
        company_id: '',
        price: '',
        stock: '',
        description: '',
        country: '',
        branch_id: ''
      },
      isLoading: true,
      isError: false,
      addModalShow: false,
      editModalShow: false,
    }
  }

  async componentDidMount() {
    this.setState({
      isLoading: true
    })

    const res = await fetch(`${Ip}:${Port}/api/admin/productList`, {
      headers: {
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmE5MDcwYzYyZTc4MDE2OTQwZmU0ZWQiLCJlbWFpbCI6InByYXRpa0BnbWFpbC5jb20iLCJwYXNzd29yZCI6ImU2ZTA2MTgzODg1NmJmNDdlMWRlNzMwNzE5ZmIyNjA5Iiwicm9sZSI6InN1YmFkbWluIiwiY29tcGFueV9pZCI6IjVmYTkwNmYyNjJlNzgwMTY5NDBmZTRlYyIsInN0YXR1cyI6ImFjdGl2ZSIsImlhdCI6MTYwODI5MjMxOX0.5p_VjgxETO3ikGxIZwIoKoKPVnb_QPEX-tuTo6wrYPU"
      }
    })
    if (res.ok) {
      const productList = await res.json()
      console.log("ProductList :: ", productList.list);
      this.setState({
        productList, isLoading: false
      })
    }
    else {
      this.setState({
        isError: true, isLoading: false
      })
    }
  }
  renderTableHeader = () => {
    let headerElement = ['id', 'name', 'images', 'price', 'stock', 'action']

    return headerElement.map((key, index) => {
      return <th key={index}>{key.toUpperCase()}</th>
    })
  }

  renderTableRows = () => {
    const removeProduct = (_id) => {
      console.log("deleting data.... ");
      axios.delete(`${Ip}:${Port}/api/admin/deleteProduct/${_id}`, {
        headers: {
          "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmE5MDcwYzYyZTc4MDE2OTQwZmU0ZWQiLCJlbWFpbCI6InByYXRpa0BnbWFpbC5jb20iLCJwYXNzd29yZCI6ImU2ZTA2MTgzODg1NmJmNDdlMWRlNzMwNzE5ZmIyNjA5Iiwicm9sZSI6InN1YmFkbWluIiwiY29tcGFueV9pZCI6IjVmYTkwNmYyNjJlNzgwMTY5NDBmZTRlYyIsInN0YXR1cyI6ImFjdGl2ZSIsImlhdCI6MTYwODI5MjMxOX0.5p_VjgxETO3ikGxIZwIoKoKPVnb_QPEX-tuTo6wrYPU"
        }
      })
        .then(async (res) => {
          console.log("response in delete  : ", res);
          console.log("response.status  :: ", res.statusText);
          if (res.statusText === "OK") {
            await Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Product has been deleted Successfully!',
              showConfirmButton: true,
              timer: 2500
            })
          } else {
            console.log("Not Deleted.. ");
            Swal.fire({
              'icon': 'error',
              'title': 'Oops...',
              "text": 'Product not Deleted.',
              'footer': 'Please Try Again!'
            })
          }
          window.location.reload();
        })
    }

    return this.state.productList.list.map(list => {
      return (
        <tr key={list._id}>
          <td>{list._id}</td>
          <td>{list.name}</td>
          <td><img src={list.images[0]} /></td>
          <td>{list.price}</td>
          <td>{list.stock}</td>
          <td>
            <Button primary onClick={() => this.setState({
              editModalShow: true, addModalShow: false,
              _id: list._id,
              name: list.name,
              images: list.images,
              images2: list.images2,
              stock: list.stock,
              price: list.price,
              category_id: list.category_id,
              company_id: list.company_id,
              cartoon_capicity: list.cartoon_capicity,
              description: list.description,
              country: list.country,
              branch_id: list.branch_id,
            })}>Edit</Button>

            <Button secondary onClick={() => removeProduct(list._id)}>Delete</Button>
          </td>
        </tr>
      )
    })
  }

  render() {
    const { productList, isLoading, isError, _id, name, images, images2, stock, price, category_id, company_id, cartoon_capicity, description, branch_id, country } = this.state
    let addModalClose = () => this.setState({ addModalShow: false })
    let editModalClose = () => this.setState({ editModalShow: false })

    if (isLoading) {
      return <div>Loading...</div>
    }

    if (isError) {
      return <div>Error...</div>
    }
    return productList.list.length > 0
      ? (
        <>
          <h1>Product List</h1>

          <Button primary className="btn" onClick={() => this.setState({ addModalShow: true, editModalShow: false })}>Add Product</Button>
          <AddProduct
            show={this.state.addModalShow}
            onHide={addModalClose}
          />
          <EditProduct
            show={this.state.editModalShow}
            onHide={editModalClose}
            id={this.state.id}
            productList={this.state.productList.list}
            _id={_id}
            name={name}
            images={images}
            images2={images2}
            stock={stock}
            price={price}
            category_id={category_id}
            cartoon_capicity={cartoon_capicity}
            company_id={company_id}
            description={description}
            country={country}
            branch_id={branch_id}
          />

          <Table singleLine lists={this.state.productList.list}>
            <Table.Header>
              <Table.Row>
                {this.renderTableHeader()}
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {this.renderTableRows()}
            </Table.Body>
          </Table>
        </>
      ) : (
        <div>
          No List...
        </div>
      )
  }
}