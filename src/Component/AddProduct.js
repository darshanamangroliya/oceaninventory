import React, { Component } from 'react'
import Modal from "react-bootstrap/Modal";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button } from 'react-bootstrap'
import { Form, Input, Select } from "antd";
import "antd/dist/antd.css";
import './form.css';
import axios from 'axios';
import Swal from 'sweetalert2';
import ImageUploader from 'react-images-upload';


var Ip = `http://18.216.87.100`
var Port = `3000`

export default class AddProduct extends Component {

    fileObj = [];
    fileArray = [];
    imageArray = []
    options = [
        {
            label: "Home&Kitchen",
            value: "5fa9078962e78016940fe4f0"
        },
        {
            label: "Beauty",
            value: "5fa9084e62e78016940fe4f2"
        }
    ]
    companyOption = [
        {
            label: "Ocean Enterprise",
            value: "5fa906f262e78016940fe4ec"
        }
    ]
    branchOption = [
        {
            label: "Katargam",
            value: "5fa9075a62e78016940fe4ef"
        }
    ]

    state = {
        selectedFile: null
    }

    constructor(props) {
        super(props)
        this.state = {
            pictures: []
        }

        this.handleChange = this.handleChange.bind(this);
        this.fileSelectHandler = this.fileSelectHandler.bind(this)

    }

    handleChange(e) {
        console.log("Id ::  ", e);
        this.setState({ id: e });
    }

    // fileSelectHandler = e => {
    //     this.fileObj.push(e.target.files)
    //     for (let i = 0; i < this.fileObj[0].length; i++) {
    //         this.fileArray.push(URL.createObjectURL(this.fileObj[0][i]))
    //     }
    //     this.setState({ file: this.fileArray })

    //     const arrayData = [];
    //     for (var i = 0; i < e.target.files.length; i++) {
    //         arrayData.push(e.target.files[i])
    //     }
    //     console.log('arrayData :: ', arrayData);
    //     this.setState({
    //         selectedFile: arrayData
    //     })
    // }

    fileSelectHandler(pictureFiles, pictuerDataURLs) {

        this.setState({
            pictures: pictureFiles
        })
    }

    fileUploadHandler = (e) => {
        e.preventDefault()
        console.log(this.state.file)

        const fd = new FormData();

        // for (var i = 0; i < this.state.selectedFile.length; i++) {
        //     fd.append('profile', this.state.selectedFile[i]);
        // }

        for (var i = 0; i < this.state.pictures.length; i++) {
            fd.append('profile', this.state.pictures[i]);
        }

        console.log('fd : ', fd);
        axios({
            url: `${Ip}:${Port}/upload`,
            method: 'POST',
            data: fd,
            headers: {
                Accept: '*/*',
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            console.log("response :: ", res);
            res.data.map(r => {
                this.imageArray.push(r)
            })
            Swal.fire(
                'Good job!',
                'Images has been Uploaded Succesfully!',
                'success'
            )
        }).catch(err => {
            console.log("Error :: ", err);
            Swal.fire({
                'icon': 'error',
                'title': 'Oops...',
                "text": 'Image Upload Error.',
                'footer': 'Please try again!!'
            })
        });
    }

    handleSubmit = (values) => {
        console.log("values", values)
        console.log("company_id", values.company_id)

        const bodyData = JSON.stringify({
            name: values.name,
            images: this.imageArray,
            category_id: values.category_id,
            company_id: values.company_id,
            price: values.price,
            sku: values.sku,
            cartoon_capicity: values.cartoon_capicity,
            stock: values.stock,
            country: values.country,
            description: values.description,
            branch_id: values.branch_id,
            amazon_url: values.amazon_url,
        });

        axios.post(`${Ip}:${Port}/api/admin/addProduct`, bodyData, {
            headers: {
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmE5MDcwYzYyZTc4MDE2OTQwZmU0ZWQiLCJlbWFpbCI6InByYXRpa0BnbWFpbC5jb20iLCJwYXNzd29yZCI6ImU2ZTA2MTgzODg1NmJmNDdlMWRlNzMwNzE5ZmIyNjA5Iiwicm9sZSI6InN1YmFkbWluIiwiY29tcGFueV9pZCI6IjVmYTkwNmYyNjJlNzgwMTY5NDBmZTRlYyIsInN0YXR1cyI6ImFjdGl2ZSIsImlhdCI6MTYwODYxNzQzOX0.yLlOv_NECjClvczrgAiuUTjCI0qXER9QZrBZiy6sEeU",
                'Accept': 'application/json',
                'content-type': 'application/json'
            },
        })
            .then(async (response) => {
                console.log("response type   :: ", typeof response.statusText);
                console.log("response.status  :: ", response.statusText);
                console.log("response  :: ", response);
                if (response.statusText === "OK") {
                    await Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Product has been Added Successfully!',
                        showConfirmButton: true,
                        timer: 1500
                    })
                    setInterval(() => {
                        window.location.reload();
                    }, 2 * 1000);
                } else {
                    console.log("Not Matched.. ");
                    Swal.fire({
                        'icon': 'error',
                        'title': 'Oops...',
                        "text": 'product not saved.',
                        'footer': 'Please Provide All Data!'
                    })
                }
            }).catch(error => {
                console.log("Some Error::", error);
                Swal.fire({
                    'icon': 'error',
                    'title': 'Oops...',
                    "text": 'product not saved.',
                    'footer': 'Please Provide All Data!'
                })
            });
    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header >
                    <Modal.Title id="contained-modal-title-vcenter">
                        Add Product
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onFinish={this.handleSubmit} className="form"
                        labelCol={{
                            span: 4
                        }}
                        wrapperCol={{
                            span: 14
                        }}
                    >
                        <Form.Item label="Select Image">
                            {/* <input type="file" onChange={this.fileSelectHandler} multiple /> */}

                            <ImageUploader
                                withIcon={true}
                                withPreview={true}
                                buttonText='Choose images'
                                onChange={this.fileSelectHandler}
                                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                maxFileSize={5242880}
                            />
                            <Button className="btn-btn" onClick={this.fileUploadHandler}>Upload</Button>

                            {/* <div className="form-group multi-preview">
                                {(this.fileArray || []).map(url => (
                                    <img src={url} alt="..." />
                                ))}
                            </div> */}

                        </Form.Item>
                        <Form.Item name="name" label="Product Name">
                            <Input placeholder="Product Name" />
                        </Form.Item>
                        <Form.Item name="category_id" label="Category ID">
                            <Select onChange={this.handleChange} placeholder="Choose Option" >
                                {this.options.map((option) => (
                                    <option name="category_id" value={option.value}>{option.label}</option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item name="company_id" label="Company ID">
                            <Select onChange={this.handleChange} placeholder="Choose Option" >
                                {this.companyOption.map((option) => (
                                    <option name="company_id" value={option.value}>{option.label}</option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item name="price" label="Price" >
                            <Input placeholder="Price" />
                        </Form.Item>
                        <Form.Item name="sku" label="Sku">
                            <Input placeholder="Sku" />
                        </Form.Item>
                        <Form.Item name="cartoon_capicity" label="Cartoon Capicity">
                            <Input placeholder="Cartoon Capicity" />
                        </Form.Item>
                        <Form.Item name="stock" label="Stock">
                            <Input placeholder="Stock" />
                        </Form.Item>
                        <Form.Item name="country" label="Country">
                            <Input placeholder="Country" />
                        </Form.Item>
                        <Form.Item name="description" label="Description">
                            <Input placeholder="Description" />
                        </Form.Item>
                        <Form.Item name="branch_id" label="Branch ID">
                            <Select onChange={this.handleChange} placeholder="Choose Option" >
                                {this.branchOption.map((option) => (
                                    <option name="branch_id" value={option.value}>{option.label}</option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item name="amazon_url" label="Amazon Url">
                            <Input placeholder="Amazon Url" />
                        </Form.Item>
                        <Form.Item >
                            <Button type="primary" htmlType="submit" className="button">Add Product</Button>
                            <Button variant="danger" onClick={this.props.onHide} className="btn1" >Close</Button>
                        </Form.Item>
                    </Form>
                </Modal.Body>
            </Modal>
        )
    }
}



