import React, { Component } from 'react';
import Modal from "react-bootstrap/Modal";
import { Button } from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import { Form, Input, message, Select } from "antd";
import "antd/dist/antd.css";
import Swal from 'sweetalert2';
import axios from 'axios';
import ImageUploader from 'react-images-upload';


var Ip = `http://18.216.87.100`;
var Port = `3000`;

export default class EditProduct extends Component {

    options = [
        {
            label: "Home&Kitchen",
            value: "5fa9078962e78016940fe4f0"
        },
        {
            label: "Beauty",
            value: "5fa9084e62e78016940fe4f2"
        }
    ]
    companyOption = [
        {
            label: "Ocean Enterprise",
            value: "5fa906f262e78016940fe4ec"
        }
    ]
    branchOption = [
        {
            label: "Katargam",
            value: "5fa9075a62e78016940fe4ef"
        }
    ]


    // imageObj = [];
    imageArray = [];
    imageArray2 = [];

    constructor(props) {
        super(props)
        this.state = {
            _id: props._id,
            name: props.name,
            images: this.imageArray,
            images2: this.imageArray2,
            stock: props.stock,
            price: props.price,
            category_id: props.category_id,
            cartoon_capicity: props.cartoon_capicity,
            company_id: props.company_id,
            description: props.description,
            country: props.country,
            branch_id: props.branch_id,
            pictures: [],
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.fileSelectHandler = this.fileSelectHandler.bind(this);

    }

    handleFieldChange = (e) => {
        console.log("Id ::  ", e);
        this.setState({ category_id: e });
    }

    handleChange = (e) => {
        let data = this.state;
        data[e.target.id] = e.target.value;
        console.log("data :: ", data.value);
        console.log("state :: ", this.state);
    }

    fileSelectHandler(pictureFiles, pictuerDataURLs) {

        /* this.imageObj.push(e.target.pictures);
         for (let i = 0; i < this.imageObj[0].length; i++) {
             this.imageArray.push(URL.createObjectURL(this.imageObj[0][i]))
         }
         this.setState({ file: this.imageArray2 });*/

        this.setState({
            pictures: pictureFiles
        })

        /* const imageArray = [];
         for (var i = 0; i < e.target.pictures.length; i++) {
             imageArray.push(e.target.pictures[i])
         }

         this.setState({
             selectedFile: imageArray
         })*/

    }

    fileUploadHandler = (e) => {
        e.preventDefault();
        const fd = new FormData();

        /*for (var i = 0; i < this.state.selectedFile.length; i++) {
            fd.append('profile', this.state.selectedFile[i]);
        }*/

        for (var i = 0; i < this.state.pictures.length; i++) {
            fd.append('profile', this.state.pictures[i]);
        }
        console.log('fd : ', fd);

        axios({
            url: `${Ip}:${Port}/upload`,
            method: 'POST',
            data: fd,
            headers: {
                Accept: '*/*',
                'Content-Type': 'multipart/form-data'
            }
        }).then(res => {
            console.log("response :: ", res);
            console.log("res :: ", res.data)
            res.data.map(r => {
                for (var i = 0; i < res.data.length; i++) {
                    // this.imageArray.push(`${Ip}:${Port}/${res.data}`);
                    this.imageArray.push(res.data);
                    this.imageArray2.push(res.data);
                    console.log("img :: ", `${Ip}:${Port}/${res.data}`);
                    console.log("img2 :: ", res.data);
                }
            })
            Swal.fire(
                'Good job!',
                'Images has been Uploaded Succesfully!',
                'success'
            )
        }).catch(err => {
            console.log("Error :: ", err);
            Swal.fire({
                'icon': 'error',
                'title': 'Oops...',
                "text": 'Image Upload Error.',
                'footer': 'Please try again!!'
            })
        });
    }


    EditProduct = () => {
        console.log("images :: ", this.imageArray)
        const data = {
            _id: this.state._id,
            name: this.state.name,
            images: this.state.images,
            images2: this.state.images2,
            category_id: this.state.category_id,
            cartoon_capicity: this.state.cartoon_capicity,
            company_id: this.state.company_id,
            price: this.state.price,
            stock: this.state.stock,
            description: this.state.description,
            branch_id: this.state.branch_id,
            country: this.state.country,
        }
        console.log("images2 :: ", this.state.images2);
        console.log("calling api :: ", this.state);
        axios.put(`${Ip}:${Port}/api/admin/productUpdate/${this.state._id}`, /*this.state,*/ data, {
            "headers": {
                "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmE5MDcwYzYyZTc4MDE2OTQwZmU0ZWQiLCJlbWFpbCI6InByYXRpa0BnbWFpbC5jb20iLCJwYXNzd29yZCI6ImU2ZTA2MTgzODg1NmJmNDdlMWRlNzMwNzE5ZmIyNjA5Iiwicm9sZSI6InN1YmFkbWluIiwiY29tcGFueV9pZCI6IjVmYTkwNmYyNjJlNzgwMTY5NDBmZTRlYyIsInN0YXR1cyI6ImFjdGl2ZSIsImlhdCI6MTYwODYxNzQzOX0.yLlOv_NECjClvczrgAiuUTjCI0qXER9QZrBZiy6sEeU",
                "content-type": "application/json"
            }
        })
            .then(async (res) => {
                console.log("res :: ", res)
                if (res.statusText === "OK") {
                    await Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Product has been Edited Successfully!',
                        showConfirmButton: true,
                        timer: 1500
                    })
                    // setInterval(() => {
                    //     window.location.reload(this.state);
                    // }, 2 * 1000);
                } else {
                    console.log("Not Matched.. ");
                    Swal.fire({
                        'icon': 'error',
                        'title': 'Oops...',
                        "text": 'product not Edited.',
                        'footer': 'Please Check All Data!'
                    })
                }
            }).catch(error => {
                console.log("Some Error::", error);
                Swal.fire({
                    'icon': 'error',
                    'title': 'Oops...',
                    "text": 'product not Edited.',
                    'footer': 'Please Check All Data!'
                })
            });
    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header >
                    <Modal.Title id="contained-modal-title-vcenter">
                        Edit Product
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onFinish={this.EditProduct} className="form"
                        labelCol={{
                            span: 4
                        }}
                        wrapperCol={{
                            span: 14
                        }}
                    >
                        <Form.Item label="Select Images" name="images" >
                            {/* <input type="file" onChange={this.fileSelectHandler} Value={this.props.images} multiple /> */}

                            <ImageUploader
                                withIcon={true}
                                withPreview={true}
                                buttonText='Choose images'
                                onChange={this.fileSelectHandler}
                                imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                maxFileSize={5242880}
                            />
                            <Button className="btn-btn" onClick={this.fileUploadHandler} >Upload</Button>

                            {/* <div className="form-group multi-preview">
                                {(this.imageArray || []).map(url => (
                                    <img src={url} alt="..." />

                                ))}
                            </div> */}

                        </Form.Item>
                        <Form.Item label="Product ID" name="_id">
                            <Input defaultValue={this.props._id} onChange={this.handleChange} disabled />
                        </Form.Item>
                        <Form.Item label="Name" name="name" >
                            <Input defaultValue={this.props.name} onChange={this.handleChange} />
                        </Form.Item>
                        <Form.Item label="Category ID" name="category_id" >
                            <Select onChange={this.handleFieldChange} defaultValue={this.props.category_id} >
                                {this.options.map((option) => (
                                    <option name="category_id" value={option.value}>{option.label}</option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Company ID" name="company_id">
                            <Select onChange={this.handleFieldChange} defaultValue={this.props.company_id} >
                                {this.companyOption.map((option) => (
                                    <option name="company_id" value={option.value}>{option.label}</option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item label="Price" name="price">
                            <Input defaultValue={this.props.price} onChange={this.handleChange} />
                        </Form.Item>
                        <Form.Item label="Cartoon Capicity" name="cartoon_capicity">
                            <Input defaultValue={this.props.cartoon_capicity} onChange={this.handleChange} />
                        </Form.Item>
                        <Form.Item label="Stock" name="stock">
                            <Input defaultValue={this.props.stock} onChange={this.handleChange} />
                        </Form.Item>
                        <Form.Item label="Country" name="country">
                            <Input defaultValue={this.props.country} onChange={this.handleChange} />
                        </Form.Item>
                        <Form.Item label="Description" name="description" >
                            <Input defaultValue={this.props.description} onChange={this.handleChange} />
                        </Form.Item>
                        <Form.Item label="Branch ID" name="branch_id">
                            <Select onChange={this.handleFieldChange} defaultValue={this.props.branch_id}  >
                                {this.branchOption.map((option) => (
                                    <option name="branch_id" value={option.value}>{option.label}</option>
                                ))}
                            </Select>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="button" onClick={this.props.onHide} >Save Changes</Button>
                            <Button variant="danger" className="btn1" onClick={this.props.onHide}>Close</Button>
                        </Form.Item>
                    </Form>
                </Modal.Body>
            </Modal >
        )
    }
}



